<?php

/**
 * Plugin Name:       Toy Options
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-options
 * Description:       Adds options for Google site verification and HTML minification.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Overrideable elementary HTML compress
if ( ! function_exists( 'toy_minify' ) ) :
	function toy_minify( $html ) {
		// Remove comments
		$html = preg_replace( '/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $html );

		// Remove white space
		$html = preg_replace( '/>\s+</', '><', $html );

		return $html;
	}
endif;

add_action(
	'wp_loaded',
	function() {
		if ( ! is_admin() ) {
			$option = get_option( 'toy_minify', true );

			if ( $option ) {
				ob_start( 'toy_minify' );
			}
		}
	},
	67
);

add_action(
	'wp_head',
	function() {
		$option = get_option( 'toy_verify', false );

		if ( $option ) {
			printf( '<meta name="google-site-verification" content="%s">', esc_html( $option ) );
		}
	},
	99
);

add_action(
	'customize_register',
	function( $wp_customize ) {
		// Add own section first
		$wp_customize->add_section(
			'toy_section',
			array(
				'title' => __( 'Toy Options' ),
			)
		);

		$wp_customize->add_setting(
			'toy_verify',
			array(
				'default'           => '',
				'sanitize_callback' => 'sanitize_text_field',
				'type'              => 'option',
				'transport'         => 'refresh',
			)
		);

		$wp_customize->add_control(
			'toy_verify',
			array(
				'label'   => 'Google verification ID',
				'section' => 'toy_section',
			)
		);

		$wp_customize->add_setting(
			'toy_minify',
			array(
				'default'   => '1',
				'type'      => 'option',
				'transport' => 'refresh',
			)
		);

		$wp_customize->add_control(
			'toy_minify',
			array(
				'label'   => 'Minify HTML?',
				'section' => 'toy_section',
				'type'    => 'checkbox',
			)
		);
	},
	67
);
