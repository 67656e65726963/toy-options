=== Toy Options ===
Contributors: thewhodidthis
Tags: compress, html, minify, minification, google, meta, verify
Requires at least: 3.0.1
Tested up to: 5.5
Stable tag: 0.0
License: Unlicense
License URI: http://unlicense.org/

Adds options for Google site verification via meta tag and for compressing front facing HTML output.
